##########################################################################################################
		
		EP1 - Orientação a Objetos
		Aluno: João Pedro Sconetto
		Matrícula: 14/0145940

		Professor: Renato Coral 
		
		Implementação de um cliente seguro, com uso de criptografica simétrica e assimétrica, para envio de imagens.

##########################################################################################################
Localização das Pastas:
	Objetos: obj/
		-Armazena os objetos criados pelas classes.
	Headers: inc/
		-Armazena os headers das classes.
	Arquivos de Execução: src/
		-Armazena arquivos com instruções de execução e a main.cpp (Arquivo principal).
	Binários: bin/
		-Armazena o arquivo binário de execução.
	Documentos: doc/
		-Armazena arquivos documentais diversos relacionados ao programa.
	Arquivos Usados: files/
		-Armazena arquivos que são usados em leituras e escrita.
	Pasta raíz: /
		-Armazena o README.md, PDF com instruções do trabalho e o Makefile para compilação e funcionamento do cliente seguro.

##########################################################################################################
Instruções para execução do Cliente:
	
	1. Assegure-se de ter instalado o programa GNU make (no terminal de execução), onde o GNU make é um programa de utilidade que determina o modo de compilação do seu programa.
	
	2. Para uma execução sem problemas sempre faça a limpeza dos Binários e dos Objetos.
		-Para fazer a limpeza dos binários e objetos digite em seu terminal:
		
					$ make clean

		-Se não lhe for apresentado nenhuma mensagem de erro a limpeza ocorreu normalmente.

	3. Após feito a limpeza o seu programa está pronto para ser compilado:
		-Para compilar o programa digite em seu terminal:
			
					$ make

		-Se não lhe for aprensentado nenhuma mensagem de erro a compilação ocorreu normalmente.
	
	4. Após feito a limpeza e compilação o seu programa está pronto para ser executado:
		-Para executar o programa digite em seu terminal:
					
					$ make run

		-O programa deve executar perfeitamente, caso apresente algum erro de execução (runtime error) identifique o erro e repasse ao desenvolvedor.

	5. Caso o programa execute sem nenhum erro de execução o cliente deve criar uma imagem de nome "imagem.jpg" que se encontrar na pasta files/.
		-OBS: Caso deseje reexecutar o programa repita todos os passos citados acima.

##########################################################################################################
