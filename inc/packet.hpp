#ifndef PACOTE_H
#define PACOTE_H

#include "array.hpp"

using namespace std;

class Packet{
	private:
		array::array *packet;
		byte tag;
		array::array *value;
	public:
		Packet();
		Packet(byte tag);
		Packet(byte tag, array::array *value);
		Packet(array::array *packet, byte tag);
		array::array *get_packet();
		array::array *get_value();
};
#endif