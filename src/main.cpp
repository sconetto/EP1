#include "array.hpp"
#include "network.hpp"
#include "packet.hpp"
#include "crypto.hpp"

#include <iostream>
#include <iomanip>
#include <ostream>
#include <istream>
#include <unistd.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>

using namespace std;

#define REQUEST_REGISTRATION 0xC0
#define REGISTRATION_START 0xC1
#define REGISTER_ID 0xC2
#define REGISTERED 0xC3

#define REQUEST_AUTH 0xA0 
#define AUTH_START 0xA1
#define REQUEST_CHALLENGE 0xA2
#define CHALLENGE 0xA4  
#define AUTHENTICATE 0xA5
#define AUTHENTICATED 0xA6

#define REQUEST_OBJECT 0xB0
#define OBJECT 0xB1

#define NOT_AUTHENTICATED 0xE0
#define OBJECT_NOT_FOUND 0xE1
#define INVALID_KEY 0xE2
#define ALREADY_REGISTERED 0xE3
#define FAILED_REGISTRATION 0xE4
#define NOT_AUTHORIZED 0xEF
#define SERVER_ERROR 0xE5

union tamanho_packet{
	int tamanho;
	byte tamanho_byte[4];
};

typedef union tamanho_interno{
	int tamanho;
	byte tamanho_byte[2];
}TamanhoInterno;

int main(int argc, char const *argv[]){
	int fd;
	array::array *packet_registration_start;
	array::array *packet_registered;
	array::array *packet_auth_start;
	array::array *packet_challenge;
	array::array *packet_authenticated;
	array::array *packet_objeto;
	array::array *value_registered;
	array::array *token_A_descriptado;
	array::array *chave_S_descriptada;
	array::array *M_descriptado;
	array::array *cabecalho;
	array::array *token_T_descriptado;
	array::array *objeto_descriptado;

	array::array *ID = array::create(8);
	ID->data[0] = 0x18;
	ID->data[1] = 0x82;
	ID->data[2] = 0x13;
	ID->data[3] = 0xD9;
	ID->data[4] = 0x93;
	ID->data[5] = 0xB2;
	ID->data[6] = 0xBC;
	ID->data[7] = 0x34;

	array::array *ID_objeto = array::create(8);
	ID_objeto->data[0] = 0x01;     
	ID_objeto->data[1] = 0x02;     
	ID_objeto->data[2] = 0x03;     
	ID_objeto->data[3] = 0x04;     
	ID_objeto->data[4] = 0x05;     
	ID_objeto->data[5] = 0x06;     
	ID_objeto->data[6] = 0x07;     
	ID_objeto->data[7] = 0x08;     

	RSA *chave_privada_cliente = crypto::rsa_read_private_key_from_PEM("files/private.pem");
	RSA *chave_publica_servidor  = crypto::rsa_read_public_key_from_PEM("files/server_pk.pem");

	array::array *ID_criptado = crypto::rsa_encrypt(ID, chave_publica_servidor);

	Packet *packet_request_registration;
	packet_request_registration = new Packet(REQUEST_REGISTRATION);
	Packet *packet_registration_start_auth;
	Packet *packet_register_id;
	Packet *packet_registered_auth;
	Packet *packet_request_auth;
	Packet *packet_token;
	Packet *packet_auth_start_auth;
	Packet *packet_request_challenge;
	Packet *packet_challenge_auth;
	Packet *packet_authenticate;
	Packet *packet_authenticated_auth;
	Packet *packet_request_object;
	Packet *packet_objeto_auth;

	union tamanho_packet packet_registered_union;
	union tamanho_packet packet_auth_start_union;
	union tamanho_packet packet_challenge_union;
	union tamanho_packet packet_authenticated_union;
	union tamanho_packet packet_objeto_union;

	printf("\n\n");
	system("clear");
	cout <<  "\t\tEP1 - Orientação a Objetos" << endl;
	cout <<	 "\t\tProfessor: Renato Sampaio Coral" << endl;
	cout <<	 "\t\tAluno: João Pedro Sconetto" << endl;
	cout <<	 "\t\tMatricula: 14/0145940" << endl;
	printf("\n\n");
	sleep(3);
	cout << "Implementação de um cliente seguro, com uso de criptografica (simétrica e assimétrica) para envio de imagens" << endl;
	printf("\n\n");
	cout << "CLIENTE SEGURO - Pronto para comunicação" << endl; 
	printf("\n\n");
	cout << "Iniciando a comunicação com o servidor..." <<endl;
	sleep(2);

	fd = network::connect("45.55.185.4", 3000);
	if(fd < 0){
		cout << "Falha na conexão" << fd << endl;
	}else{
		cout << "Conexão OK!" << endl;
		
		cout << "PROTOCOLO DE REGISTRO" << endl;
		sleep(2);
		network::write(fd, packet_request_registration->get_packet());
		if((packet_registration_start = network::read(fd))== nullptr){
			cout << "Leitura nula" << endl;
		}else{
			packet_registration_start_auth = new Packet(packet_registration_start, REGISTRATION_START);
			if(packet_registration_start_auth->get_packet() == NULL){
				cout << "Erro, hash ou tag diferentes" << endl; 
			}else{
				cout << "Pacote esperado recebido com sucesso!" << endl;	
			}
			packet_register_id = new Packet(REGISTER_ID, ID_criptado);
			sleep(2);
			printf("\n");
			network::write(fd, packet_register_id->get_packet());

			union tamanho_packet packet_registered_union;
			if((cabecalho = network::read(fd, 4)) == nullptr){
				cout << "Erro na leitura do pacote" << endl;
			}else{
				packet_registered_union.tamanho_byte[0] = cabecalho->data[0];
				packet_registered_union.tamanho_byte[1] = cabecalho->data[1];
				packet_registered_union.tamanho_byte[2] = cabecalho->data[2];
				packet_registered_union.tamanho_byte[3] = cabecalho->data[3];
				if((packet_registered = network::read(fd, packet_registered_union.tamanho)) == nullptr){
					cout << "Erro na leitura do pacote" << endl;	
				}else{
					cout << "Imprimindo conteudo do pacote recebido REGISTERED: " << *packet_registered << endl;
					sleep(2);
					printf("\n");
					packet_registered_auth = new Packet(packet_registered, REGISTERED);
					array::array *chave_S = packet_registered_auth->get_value();
					chave_S_descriptada = crypto::rsa_decrypt(chave_S, chave_privada_cliente);
				}
			}
		}
	}
	system("clear");
	cout << "PROTOCOLO DE AUTENTICAÇÃO\n" << endl;
	sleep(2);
	packet_request_auth = new Packet(REQUEST_AUTH, ID_criptado);
	network::write(fd, packet_request_auth->get_packet());

	array::destroy(cabecalho);
	if((cabecalho = network::read(fd, 4)) == nullptr){
		cout << "Erro na leitura do pacote" << endl;
	}else{
		packet_auth_start_union.tamanho_byte[0] = cabecalho->data[0];
		packet_auth_start_union.tamanho_byte[1] = cabecalho->data[1];
		packet_auth_start_union.tamanho_byte[2] = cabecalho->data[2];
		packet_auth_start_union.tamanho_byte[3] = cabecalho->data[3];

		if((packet_auth_start = network::read(fd, packet_auth_start_union.tamanho)) == nullptr){
			cout << "Erro na leitura do pacote" << endl;	
		}
else{
			cout << "Imprimindo conteudo do pacote recebido AUTH_START: " << *packet_auth_start << endl;
			sleep(2);
			printf("\n");
			packet_auth_start_auth = new Packet(packet_auth_start, AUTH_START);
			array::array *token_A = packet_auth_start_auth->get_value();
			token_A_descriptado = crypto::rsa_decrypt(token_A, chave_privada_cliente);
		}
		packet_request_challenge = new Packet(REQUEST_CHALLENGE);
		network::write(fd, packet_request_challenge->get_packet());

		array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
			cout << "Erro na leitura do packet" << endl;
		}else{
			packet_challenge_union.tamanho_byte[0] = cabecalho->data[0];
			packet_challenge_union.tamanho_byte[1] = cabecalho->data[1];
			packet_challenge_union.tamanho_byte[2] = cabecalho->data[2];
			packet_challenge_union.tamanho_byte[3] = cabecalho->data[3];
			if((packet_challenge = network::read(fd, packet_challenge_union.tamanho)) == nullptr){
				cout << "Erro na leitura do pacote" << endl;	
			}else{
				cout << "Imprimindo conteudo do pacote recebido CHALLENGE: " << *packet_challenge << endl;
				sleep(2);
				printf("\n");
				packet_challenge_auth = new Packet(packet_challenge, CHALLENGE);
				array::array *M_criptado = packet_challenge_auth->get_value();
				M_descriptado = crypto::aes_decrypt(M_criptado, token_A_descriptado, chave_S_descriptada);
			}
		}
		packet_authenticate = new Packet(AUTHENTICATE, M_descriptado);
		network::write(fd, packet_authenticate->get_packet());
		array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
			cout << "Erro na leitura do pacote" << endl;
		}else{
			packet_authenticated_union.tamanho_byte[0] = cabecalho->data[0];
			packet_authenticated_union.tamanho_byte[1] = cabecalho->data[1];
			packet_authenticated_union.tamanho_byte[2] = cabecalho->data[2];
			packet_authenticated_union.tamanho_byte[3] = cabecalho->data[3];
			if((packet_authenticated = network::read(fd, packet_authenticated_union.tamanho)) == nullptr){
				cout << "Erro na leitura do pacote" << endl;	
			}else{
				cout << "Imprimindo conteúdo do pacote recebido AUTHENTICATED: " << *packet_authenticated << endl;
				sleep(2);
				printf("\n");

				packet_authenticated_auth = new Packet(packet_authenticated, AUTHENTICATED);
				array::array *token_T = packet_authenticated_auth->get_value();
				token_T_descriptado = crypto::aes_decrypt(token_T, token_A_descriptado, chave_S_descriptada);
			}
		}
	system("clear");
	sleep(2);
	cout << "PROTOCOLO DE REQUISICÃO!\n" << endl;
	sleep(2);
	cout << "ID DO OBJETO "<< *ID_objeto << endl;
	cout << "\nTOKEN T "<< *token_T_descriptado << endl;
	sleep(1);
	cout << "\nCHAVE S "<< *chave_S_descriptada << endl;
	sleep(1);
	array::array *ID_objeto_criptado = crypto::aes_encrypt(ID_objeto, token_T_descriptado, chave_S_descriptada);
	cout << "\nOBJETO ENCRIPTADO " << *ID_objeto_criptado << endl;
	packet_request_object = new Packet(REQUEST_OBJECT, ID_objeto_criptado);
	network::write(fd, packet_request_object->get_packet());
	array::destroy(cabecalho);
		if((cabecalho = network::read(fd, 4)) == nullptr){
			cout << "erro na leitura do pacote" << endl;
		}else{
			cout << "\nCABEÇALHO = " << *cabecalho << endl;
			packet_objeto_union.tamanho_byte[0] = cabecalho->data[0];
			packet_objeto_union.tamanho_byte[1] = cabecalho->data[1];
			packet_objeto_union.tamanho_byte[2] = cabecalho->data[2];
			packet_objeto_union.tamanho_byte[3] = cabecalho->data[3];

			if((packet_objeto = network::read(fd, packet_objeto_union.tamanho)) == nullptr){
				cout << "erro na leitura do pacote" << endl;	
			}else{
				cout << "Imprimindo conteudo do pacote recebido OBJETO: " << *packet_objeto << endl;
				sleep(2);
				printf("\n");

				packet_objeto_auth = new Packet(packet_objeto, OBJECT);
				array::array *objeto_encriptado = packet_objeto_auth->get_value();

				cout << "OBJETO "<< *objeto_encriptado << endl;
				objeto_descriptado = crypto::aes_decrypt(objeto_encriptado, token_T_descriptado, chave_S_descriptada);
			}
		}	
	}	
	ofstream imagem;
	imagem.open ("files/imagem.jpg", ios::binary);
	if( !imagem ){
		cout << "Erro ao criar o arquivo";
		return 1;
	}else{
		cout << "\nCriando o OBJETO, baixando IMAGEM!\n" << endl;
		sleep(2);
		for (int i = 0; i < ((int)objeto_descriptado->length); i++){
			imagem << objeto_descriptado->data[i];
		}
	}
	imagem.close();
	system("clear");
	cout << "Execução terminada com SUCESSO!" << endl;
	sleep(1);
	return 0;
}