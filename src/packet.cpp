#include "packet.hpp"
#include "crypto.hpp"
#include "array.hpp"

#include <iostream>

typedef union tamanho_packet{
	int tamanho;
	byte tamanho_byte[4];
}Tamanhopacket;

typedef union tamanho_packet_interno{
	int tamanho;
	byte tamanho_byte[2];
}TamanhoInterno;

Packet::Packet(byte tag){
	packet = array::create(7);
	packet -> data[0] = 0x03;
	packet -> data[1] = 0;
	packet -> data[2] = 0;
	packet -> data[3] = 0;
	packet -> data[4] = tag;
	packet -> data[5] = 0;
	packet -> data[6] = 0;
}

Packet::Packet(byte tag, array::array *value){
	array::array *hash;
	packet = array::create((int)value->length + 27);	
	hash = crypto::sha1(value);
	Tamanhopacket tamanho_packet;
	TamanhoInterno tamanho_interno; 
	tamanho_packet.tamanho = 23+(int)value->length;
	tamanho_interno.tamanho = (int)value->length;
	packet->data[0] = tamanho_packet.tamanho_byte[0];
	packet->data[1] = tamanho_packet.tamanho_byte[1];
	packet->data[2] = tamanho_packet.tamanho_byte[2];
	packet->data[3] = tamanho_packet.tamanho_byte[3];
	packet->data[4] = tag;
	packet->data[5] = tamanho_interno.tamanho_byte[0];
	packet->data[6] = tamanho_interno.tamanho_byte[1];
	
	memcpy(packet->data + 7, value->data, value->length);
	memcpy(packet->data + 7 + value->length, hash->data, 20);
}

Packet::Packet(array::array *packet_recebido, byte tag){
	byte tag_recebido;
	array::array *length_recebido = array::create(2);
	array::array *value_recebido;
	array::array *hash_recebido = array::create(20);
	if(packet_recebido->data[0] != tag){}
	else{
		tag_recebido = packet_recebido->data[0];
		length_recebido->data[0] = packet_recebido->data[1];
		length_recebido->data[1] = packet_recebido->data[2];
		TamanhoInterno tamanho_interno;
		tamanho_interno.tamanho_byte[0] = length_recebido->data[0];
		tamanho_interno.tamanho_byte[1] = length_recebido->data[1];

		value_recebido = array::create(tamanho_interno.tamanho);
		for(int i = 3, j=0; i < (tamanho_interno.tamanho+3), j< tamanho_interno.tamanho;i++, j++){
			value_recebido->data[j] = packet_recebido->data[i];
		}
		for(int i = tamanho_interno.tamanho, f = 0; i < (tamanho_interno.tamanho + 20), f < 20; i++, f++){
			hash_recebido->data[f] = packet_recebido->data[i];
		}
	this->value = value_recebido;
	}
}

array::array *Packet::get_packet(){
	return packet;
}
array::array *Packet::get_value(){
	return value;
}